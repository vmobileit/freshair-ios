//
//  DamperCell.swift
//  FreshAir
//
//  Created by Easyway_Mac2 on 30/04/19.
//  Copyright © 2019 Easyway_Mac2. All rights reserved.
//

import UIKit

protocol DamperCellDelegate: class {
    func didTapSwitchView(sender: DamperCell)
}

class DamperCell: UITableViewCell {

    @IBOutlet var roomNameLabel: UILabel!
    @IBOutlet var switchView: UIView!
    @IBOutlet var damperSwitch: UISwitch!
    
    weak var delegate: DamperCellDelegate?
    
    var damperParam: DamperParams? {
        didSet {
            if let roomName = damperParam?.name {
                roomNameLabel.text = roomName
            }
            
            if let status = damperParam?.status {
                damperSwitch.isOn = status == "0" ? false : true
            }
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapDetected(sender:)))
        switchView.addGestureRecognizer(tapGesture)
    }

    @objc func tapDetected(sender: UITapGestureRecognizer) {
        delegate?.didTapSwitchView(sender: self)
    }

}
