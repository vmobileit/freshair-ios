//
//  Switcher.swift
//  FreshAir
//
//  Created by Easyway_Mac2 on 20/02/20.
//  Copyright © 2020 Easyway_Mac2. All rights reserved.
//

import UIKit

class Switcher {
    
    static func updateRootVC() {
        
        let isLoggedIn = UserDefaults().getBoolForKey(key: .isLoggedIn)
        var rootVC: UIViewController?
    
        if isLoggedIn {
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let homeNavVC = storyBoard.instantiateViewController(withIdentifier: "HomeNAV") as! HomeNavigationController
                rootVC = homeNavVC
        } else {
            rootVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LOGIN") as! LoginViewController
        }
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = rootVC
    }
}
