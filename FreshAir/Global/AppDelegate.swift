//
//  AppDelegate.swift
//  FreshAir
//
//  Created by Easyway_Mac2 on 19/06/18.
//  Copyright © 2018 Easyway_Mac2. All rights reserved.
//

import UIKit
import SVProgressHUD

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        Switcher.updateRootVC()
        
        SVProgressHUD.setDefaultMaskType(.black)
        return true
    }
}
