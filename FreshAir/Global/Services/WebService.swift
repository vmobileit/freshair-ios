//
//  WebService.swift
//  FreshAir
//
//  Created by Easyway_Mac2 on 13/02/20.
//  Copyright © 2020 Easyway_Mac2. All rights reserved.
//

import Foundation
import Alamofire
import SVProgressHUD

enum ServiceType: String {
    case data = "/getdata.cmd"
    case dampers = "/getdmprs.cmd"
}

enum FanActionType {
    case regulator
    case fanSwitch
}

protocol ResultProtocol: class {
    func success(_ responseData: Data)
    func error(_ message: String)
}

protocol FanResultProtocol: class {
    func executionStatus(_ status: Bool, with level: String)
}

class WebService {
    
    weak var delegate: ResultProtocol?
    weak var fanDelegate: FanResultProtocol?
    
    //MARK: - CHECK NETWORK CONNECTION
    
    func isConnectedToNetwork() -> Bool {
        let networkReachability = NetworkReachability()
        return networkReachability.isConnectedToNetwork()
    }
    
    func getData(for ipAddress: String, serviceType: ServiceType) {
        
        if !isConnectedToNetwork() {
            SVProgressHUD.dismiss()
            self.delegate?.error("The Internet connection appears to be offline.")
            return
        }
        
        let url = "http://\(ipAddress)" + serviceType.rawValue
        
        var request = URLRequest(url: URL(string: url)!)
        request.timeoutInterval = 30
        
         AF.request(request)
                   .responseJSON { [weak self] (response) in
                    
                    SVProgressHUD.dismiss()
                    
                       switch response.result {
                       case .success(_):
                        print("Response", response.value as Any)
                           if let data = response.data, response.response?.statusCode == 200 {
                            self?.delegate?.success(data)
                           } else {
                               self?.delegate?.error("Unable to reach server")
                           }
                       case .failure( _):
                           self?.delegate?.error("Unable to reach server")
                       }
               }
    }
    
    func executeFanAction(with level: String, for ipAddress: String) {
        
        let url = "http://\(ipAddress)/execute.cmd?speed=\(level)"
        
        AF.request(url)
            .responseString { [weak self] (response) in
                switch response.result {
                case .success(_):
                    print("fanLevel", response.value as Any)
                    self?.fanDelegate?.executionStatus(true, with: level)
                case .failure( _):
                    self?.fanDelegate?.executionStatus(false, with: level)
                }
        }
    }
}
