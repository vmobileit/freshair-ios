//
//  UIConstants.swift
//  FreshAir
//
//  Created by Easyway_Mac2 on 19/06/18.
//  Copyright © 2018 Easyway_Mac2. All rights reserved.
//


import UIKit

let IS_IPAD = (UIDevice.current.userInterfaceIdiom == .pad)
let IS_IPHONE_4 = (UIScreen.main.bounds.size.height == 480)
let IS_IPHONE_5 = (UIScreen.main.bounds.size.height == 568)
let IS_IPHONE_6 = (UIScreen.main.bounds.size.height == 667)
let IS_IPHONE_61 = (UIScreen.main.bounds.size.height == 736)
let BGCOLORSELECTEDVIEWTAG = 1001
let BGSELECTEDVIEWCOLOR = UIColor(red: 42.0 / 255.0, green: 42.0 / 255.0, blue: 42.0 / 255.0, alpha: 1.0)
let CUSTOMORANGECOLOR = UIColor(red: 255.0 / 255.0, green: 101.0 / 255, blue: 26.0 / 255.0, alpha: 1.0)
let GOTHAMLIGHT = "GOTHAM-LIGHT_0.OTF"
let GOTHAMTHIN = "GOTHAM-THIN.TTF"
let TITLES_FONT1 = UIFont(name: "GOTHAM-LIGHT_0.OTF", size: UIScreen.main.bounds.size.height/35)
let VIEWBGCOLOR = UIColor(red: 18.0 / 255.0, green: 18.0 / 255.0, blue: 18.0 / 255.0, alpha: 1.0)
let URLBGCOLOR = UIColor(red: 31.0 / 255.0, green: 31.0 / 255.0, blue: 31.0 / 255.0, alpha: 1.0)
let URLBORDERCOLOR = UIColor(red: 92.0 / 255.0, green: 92.0 / 255.0, blue: 92.0 / 255.0, alpha: 0.2)
let BUTTON_BGCOLOR = UIColor(red: 29.0 / 255.0, green: 29.0 / 255, blue: 29.0 / 255.0, alpha: 1.0)
let CELL_BORDERCOLOR = UIColor(red: 92.0 / 255.0, green: 92.0 / 255.0, blue: 92.0 / 255.0, alpha: 0.2).cgColor
let CELL_BGCOLOR = UIColor(red: 31.0 / 255.0, green: 31.0 / 255.0, blue: 31.0 / 255.0, alpha: 1.0)



