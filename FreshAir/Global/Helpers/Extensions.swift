//
//  Extensions.swift
//  AegisAutomation
//
//  Created by Easyway_Mac2 on 14/11/18.
//  Copyright © 2018 vmobileit. All rights reserved.
//
import Foundation
import UIKit


extension UIColor {
    static func rgb(red: CGFloat, green: CGFloat, blue: CGFloat) -> UIColor {
        return UIColor(red: red/255, green: green/255, blue: blue/255, alpha: 1)
    }
}

extension UINavigationController {
    open override var preferredStatusBarStyle: UIStatusBarStyle {
        return topViewController?.preferredStatusBarStyle ?? .lightContent
    }
}

extension CALayer {
    var borderColorFromUIColor: UIColor {
        get {
            return UIColor(cgColor: self.borderColor!)
        } set {
            self.borderColor = newValue.cgColor
        }
    }
}

extension UserDefaults {
    enum UserKeys:String {
        
        case isLoggedIn   // user is logged in or out
        case isAppLaunchedFirstTime
        case baseUrl
       
    }
    
    func setValueForKey(value:Any,key:UserKeys){
        setValue(value, forKey: key.rawValue)
        synchronize()
    }
    
    // Always returns nil if value is not set
    func getValueForKey(key:UserKeys) -> Any {
        return value(forKey: key.rawValue) as Any
    }
    
    func setBoolForKey(value:Bool,key:UserKeys){
        set(value, forKey: key.rawValue)
        synchronize()
    }
    
    // Always returns false if bool is not set
    func getBoolForKey(key:UserKeys) -> Bool {
        return bool(forKey: key.rawValue)
    }
    
    func removeValueForKey(key:UserKeys){
        removeObject(forKey: key.rawValue)
    }
    
    func resetUserDefaults(){
        let dict = dictionaryRepresentation() as NSDictionary
        for key in dict.allKeys {
            removeObject(forKey: key as! String)
        }
        synchronize()
    }
}

extension UIViewController {
    func showAlertWithTitle(_ title: String, errorMessgage: String){
        DispatchQueue.main.async {
            let alert = UIAlertController(title: title, message: errorMessgage, preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(ok)
            self.present(alert, animated: true)
        }
    }
}

//MARK: UITABLEVIEW SET ALERT MESSGAE
extension UITableView {
    func setEmptyMessage(_ message: String) {
        let messageView = UIView(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
        
        let messageLabel = UILabel()
        messageLabel.text = message
        messageLabel.textColor = .white
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        messageLabel.font = UIFont.systemFont(ofSize: 17)
        messageView.addSubview(messageLabel)
        messageLabel.translatesAutoresizingMaskIntoConstraints = false
        messageLabel.widthAnchor.constraint(equalTo: messageView.widthAnchor, multiplier: 0.75).isActive = true
        let xConstraint = NSLayoutConstraint(item: messageLabel, attribute: .centerX, relatedBy: .equal, toItem: messageView, attribute: .centerX, multiplier: 1, constant: 0)
        
        let yConstraint = NSLayoutConstraint(item: messageLabel, attribute: .centerY, relatedBy: .equal, toItem: messageView, attribute: .centerY, multiplier: 1, constant: 0)
        NSLayoutConstraint.activate([xConstraint, yConstraint])
        self.backgroundView = messageView
        self.separatorStyle = .none
    }
}

//MARK: UICOLLECTIONVIEW SET ALERT MESSGAE
extension UICollectionView {
    func setEmptyMessage(_ message: String) {
        let messageView = UIView(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
        
        let messageLabel = UILabel()
        messageLabel.text = message
        messageLabel.textColor = .black
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        messageLabel.font = UIFont.systemFont(ofSize: 17)
        messageView.addSubview(messageLabel)
        messageLabel.translatesAutoresizingMaskIntoConstraints = false
        messageLabel.widthAnchor.constraint(equalTo: messageView.widthAnchor, multiplier: 0.75).isActive = true
        let xConstraint = NSLayoutConstraint(item: messageLabel, attribute: .centerX, relatedBy: .equal, toItem: messageView, attribute: .centerX, multiplier: 1, constant: 0)
        
        let yConstraint = NSLayoutConstraint(item: messageLabel, attribute: .centerY, relatedBy: .equal, toItem: messageView, attribute: .centerY, multiplier: 1, constant: 0)
        NSLayoutConstraint.activate([xConstraint, yConstraint])
        self.backgroundView = messageView
    }
}
