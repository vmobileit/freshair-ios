//
//  LoginViewModel.swift
//  FreshAir
//
//  Created by Easyway_Mac2 on 13/02/20.
//  Copyright © 2020 Easyway_Mac2. All rights reserved.
//

import Foundation
import SVProgressHUD

protocol LoginFunctionProtocol {
    func login(with ipAdress: String?)
}

class LoginViewModel: LoginFunctionProtocol {
    
    weak var delegate: ResultProtocol?
    
    private let webService = WebService()
    
    func login(with ipAddress: String?) {
        if let ipAddress = ipAddress, !ipAddress.isEmpty {
            UserDefaults().setValueForKey(value: ipAddress, key: .baseUrl)
            webService.delegate = delegate
            SVProgressHUD.show()
            webService.getData(for: ipAddress, serviceType: .data)
        } else {
            delegate?.error("Please Enter Valid Ip")
        }
    }
}
