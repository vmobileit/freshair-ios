//
//  ViewController.swift
//  FreshAir
//
//  Created by Easyway_Mac2 on 19/06/18.
//  Copyright © 2018 Easyway_Mac2. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    //MARK: - IBOutlets
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var contentView: UIView!
    @IBOutlet var ipTextField: UITextField!
    @IBOutlet var signInButton: UIButton!
    private let viewModel = LoginViewModel()

    private var loginResponseData: Data?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        addNotifications()
        
        let generalTapGesture = UITapGestureRecognizer(target: self, action: Selector.generalTap)
        view.addGestureRecognizer(generalTapGesture)
        
        textFieldAttributes()
        signInButton.layer.borderColor = UIColor(red: 92.0 / 255.0, green: 92.0 / 255.0, blue: 92.0 / 255.0, alpha: 0.5).cgColor
        
        /// Delegate the  Login View model protocol
        viewModel.delegate = self
        
        ///Set applaunched first time to true in user defaults
        UserDefaults().setBoolForKey(value: true, key: .isAppLaunchedFirstTime)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    private func addNotifications() {
           NotificationCenter.default.addObserver(
               self,
               selector: Selector.keyboardWillShowHandler,
               name: UIResponder.keyboardWillShowNotification,
               object: nil
           )

           NotificationCenter.default.addObserver(
               self,
               selector: Selector.keyboardWillHideHandler,
               name: UIResponder.keyboardWillHideNotification,
               object: nil
           )
       }
       
       deinit {
         NotificationCenter.default.removeObserver(self)
       }
    
    //MARK: - Keyboard Helper functions
    fileprivate func adjustInsetForKeyboard(isShow: Bool, notification: Notification) {
        guard  let value = notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue  else {
            return
        }
        let keyboardFrame = value.cgRectValue
        let adjustmentHeight = (keyboardFrame.height + 20) * (isShow ? 1 : -1)
        scrollView.contentInset.bottom += adjustmentHeight
        scrollView.scrollIndicatorInsets.bottom  += adjustmentHeight
    }

    @objc fileprivate func keyboardWillShow(notification: Notification) {
        adjustInsetForKeyboard(isShow: true, notification: notification)
    }

    @objc fileprivate func keyboardWillHide(notification: Notification) {
        adjustInsetForKeyboard(isShow: false, notification: notification)
    }
    
    @objc fileprivate func dismissKeyboard() {
        view.endEditing(true)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        dismissKeyboard()
    }

    //MARK: - IBActions
    @IBAction func signInAction(_ sender: Any) {
        dismissKeyboard()
        
        viewModel.login(with: ipTextField.text)
    }
}

//MARK: - TextField Delegate
extension LoginViewController: UITextFieldDelegate {
    
    func textFieldAttributes() {
        ipTextField.attributedPlaceholder = NSAttributedString(string: "ENTER URL",attributes: [NSAttributedString.Key.foregroundColor:UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 0.5)])
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (string.rangeOfCharacter(from:.init(charactersIn: "0123456789.")) != nil) || (range.length == 1 || string.isEmpty == true) {
            return true
        } else {
            return false
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

// MARK: - Show result
extension LoginViewController: ResultProtocol {

    func showPopup(message: String = "Something went wrong. Please try again") {
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Done", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    func success(_ responseData: Data) {
        UserDefaults.standard.setBoolForKey(value: true, key: .isLoggedIn)
        
        DispatchQueue.main.async {
            self.loginResponseData = responseData
            Switcher.updateRootVC()
            //self.performSegue(withIdentifier: "goToDashboard", sender: self)
        }
    }
    
    func error(_ message: String) {
        showPopup(message: message)
    }
}

// MARK: - Selector
fileprivate extension Selector {
    static  let  keyboardWillShowHandler = #selector(LoginViewController.keyboardWillShow(notification:))
    static  let  keyboardWillHideHandler = #selector(LoginViewController.keyboardWillHide(notification:))
    static let generalTap = #selector(LoginViewController.dismissKeyboard)
}
