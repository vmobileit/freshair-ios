//
//  DashboardVC.swift
//  FreshAir
//
//  Created by Easyway_Mac2 on 20/06/18.
//  Copyright © 2018 Easyway_Mac2. All rights reserved.
//

import UIKit
import SVProgressHUD

class DashboardVC: UIViewController {
    
    //MARK: - IBOutlets
    @IBOutlet var humidityLabel: UILabel!
    @IBOutlet var gasLabel: UILabel!
    @IBOutlet var indoorTempLabel: UILabel!
    @IBOutlet var outdoorTempLabel: UILabel!
    @IBOutlet var ppmLabel: UILabel!
    @IBOutlet var fanSwitch: UISwitch!
    @IBOutlet var levelButton: UIButton!
    @IBOutlet var switchView: UIView!
    @IBOutlet var dataView: UIView!
    @IBOutlet var errorMessageLabel: UILabel!
    
    //MARK: - Internal Parameters
    private var parametersArray: [Parameters]? = nil {
        didSet {
            guard let parameters = parametersArray?.first else {
                return }
            hideErrorMessage()
            humidityLabel.text = (parameters.humidity ?? "-") + " ppm"
            gasLabel.text = (parameters.vocGas ?? "-") + " %"
            indoorTempLabel.text = (parameters.temperature ?? "-") + " ºC"
            outdoorTempLabel.text = (parameters.outTemp ?? "-") + " ºC"
            ppmLabel.text = (parameters.ppm ?? "-") + " ppm"
            
            if let fanValue = parameters.fanValue {
                fanSwitch.isOn = fanValue == "0" ? false : true
                levelButton.setTitle("Level \(fanValue)", for: .normal)
            }
        }
    }
    
    private let viewModel = DashboardViewModel()
    
    //MARK: - External Parameters
    var loginData: Data?
    
    //MARK: - View Controller Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNavigationBar()
        viewModel.startDataTimer()
        showErrorMessgage()
        
        ///Delegate the view model
        viewModel.delegate = self
        viewModel.fanExecutionDelegate = self
        
        ///Show Activity Indicator
        SVProgressHUD.show()

        ///Parse the data and Update the UI
        //guard let data = loginData else { return }
        //parametersArray = viewModel.parseLoginResponse(of: data)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        viewModel.startDataTimer()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        viewModel.stopDataTimer()
    }
    
    //MARK: - UI Setup
    private func setNavigationBar() {
        navigationItem.title = "Fresh Air"
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white,.font:UIFont.systemFont(ofSize: 17.0, weight: .regular)]
        navigationItem.hidesBackButton = true
        
        //navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Machines", style: .plain, target: self, action: Selector.showMachines)
        
        //navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Rooms", style: .plain, target: self, action: Selector.getDampers)
    }
    
    //MARK: - Helper functions

    @objc fileprivate func getDampers() {
        let leftVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DAMPERS")
        let transition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.moveIn
        transition.subtype = CATransitionSubtype.fromLeft
        self.navigationController?.view.layer.add(transition, forKey: nil)
        self.navigationController?.pushViewController(leftVC, animated: false)
    }
    
    @objc fileprivate func handleMachinesList() {
        let leftVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MACHINES")
        self.navigationController?.pushViewController(leftVC, animated: false)
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return .none
    }
    
    private func showErrorMessgage() {
        errorMessageLabel.isHidden = false
        dataView.isHidden = true
    }
    
    private func hideErrorMessage() {
        dataView.isHidden = false
        errorMessageLabel.isHidden = true
    }
    
    //MARK: - IBActions
    @IBAction func handleLogout(_ sender: UIButton) {
        
        let alert = UIAlertController(title: "", message: "Do you want to logout?", preferredStyle: .alert)
        let ok = UIAlertAction(title: "YES", style: .default, handler: { [weak self] action in
            
            UserDefaults().removeValueForKey(key: .isLoggedIn)
            UserDefaults().removeValueForKey(key: .baseUrl)
            
            self?.viewModel.stopDataTimer()
            Switcher.updateRootVC() /*self?.navigationController?.popToRootViewController(animated: true) */
        })
        let cancel = UIAlertAction(title: "NO", style: .default, handler: { action in
            alert.dismiss(animated: true)
        })
        alert.addAction(ok)
        alert.addAction(cancel)
        present(alert, animated: true)
    }
    
    @IBAction func changeLevel(_ sender: UIButton) {
        let popOverVC: PopOverVC = storyboard?.instantiateViewController(withIdentifier: "POPOVER") as! PopOverVC
        popOverVC.modalPresentationStyle = .popover
        popOverVC.preferredContentSize = CGSize(width: 80, height: 200)
        popOverVC.delegate = self
        
        let controller = popOverVC.popoverPresentationController
        
        controller?.delegate = self
        controller?.permittedArrowDirections = [.right]
        
        controller?.sourceView = levelButton
        controller?.sourceRect = levelButton.bounds
        
        present(popOverVC, animated: true, completion: nil)
    }
    
    @IBAction func switchViewTapped(_ sender: Any) {
        if fanSwitch.isOn {
            fanExecutionAPI(level: "0", type: .fanSwitch)
        } else {
            fanExecutionAPI(level: "4", type: .fanSwitch)
        }
    }
    
    @IBAction func rightSwipeGestureAction(_ sender: UISwipeGestureRecognizer) {
        let leftVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DAMPERS")
        let transition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.moveIn
        transition.subtype = CATransitionSubtype.fromLeft
        self.navigationController?.view.layer.add(transition, forKey: nil)
        self.navigationController?.pushViewController(leftVC, animated: true)
        
    }
}

//MARK: - PopOver Delegate
extension DashboardVC: UIPopoverPresentationControllerDelegate, isAbleToReceiveData {
    
    func fanExecutionAPI(level: String, type: FanActionType) {
        viewModel.fanExecutionAPI(level: level, type: type)
    }
}


//MARK: - Result Protocol
extension DashboardVC: ResultProtocol {
    func success(_ responseData: Data) {
        parametersArray = viewModel.parseLoginResponse(of: responseData)
    }
    
    func error(_ message: String) {
        //Show error only once
    }
}

//MARK: - Fan Execution Result Protocol
extension DashboardVC: FanResultProtocol {
    func executionStatus(_ status: Bool, with level: String) {
        if !status {
            showAlertWithTitle("Error", errorMessgage: "Error executing fan command")
        } else {
            if level == "0" {
                fanSwitch.setOn(false, animated: true)
                levelButton.setTitle("Level 0", for: .normal)
            } else {
                fanSwitch.setOn(true, animated: true)
                levelButton.setTitle("Level \(level)", for: .normal)
            }
        }
    }
    
}

// MARK: - Selector
fileprivate extension Selector {
    static let getDampers = #selector(DashboardVC.getDampers)
    static let showMachines = #selector(DashboardVC.handleMachinesList)
}
/*
@objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
    if let swipeGesture = gesture as? UISwipeGestureRecognizer {
        
        switch swipeGesture.direction {
        case UISwipeGestureRecognizer.Direction.left:
            let rightVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MACHINES")
            self.navigationController?.pushViewController(rightVC, animated: true)
            
        default:
            break
        }
    }
}
*/
