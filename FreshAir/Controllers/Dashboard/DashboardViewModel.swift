//
//  DashboardViewModel.swift
//  FreshAir
//
//  Created by Easyway_Mac2 on 13/02/20.
//  Copyright © 2020 Easyway_Mac2. All rights reserved.
//

import Foundation


class DashboardViewModel {
    weak var delegate: ResultProtocol?
    weak var fanExecutionDelegate: FanResultProtocol?
    
    //MARK: - Internal Parameters
    private var dataTimer: Timer?
    
    private var baseUrl: String = {
        if let url = UserDefaults().getValueForKey(key: .baseUrl) as? String {
            return url
        } else {
            fatalError("Base Url not found")
        }
    }()
    
    private let webService = WebService()
    
    //MARK: - Data Parse
    func parseLoginResponse(of data: Data) -> [Parameters]? {
        do {
            let decoder = JSONDecoder()
            let decodedResponse = try decoder.decode([Parameters].self, from: data)
            return decodedResponse
        } catch let error {
            print(error)
        }
        return nil
    }
    
    //MARK: - Data Timer
    func startDataTimer() {
        if dataTimer == nil {
            dataTimer =  Timer.scheduledTimer(
                timeInterval: TimeInterval(3.0),
                target      : self,
                selector    : Selector.fetchData,
                userInfo    : nil,
                repeats     : true)
        }
    }
    
    func stopDataTimer() {
        if dataTimer != nil {
            dataTimer?.invalidate()
            dataTimer = nil
        }
    }
    
    @objc fileprivate func fetchData() {
        webService.delegate = delegate
        webService.getData(for: baseUrl, serviceType: .data)
    }
    
    func fanExecutionAPI(level: String, type: FanActionType) {
        webService.fanDelegate = fanExecutionDelegate
        webService.executeFanAction(with: level, for: baseUrl)
    }
}

fileprivate extension Selector {
    static  let  fetchData = #selector(DashboardViewModel.fetchData)
}
