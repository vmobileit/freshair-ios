//
//  DashboardVC.swift
//  FreshAir
//
//  Created by Easyway_Mac2 on 20/06/18.
//  Copyright © 2018 Easyway_Mac2. All rights reserved.
//

import UIKit
import SVProgressHUD

class DashboardVC: UIViewController, UIPopoverPresentationControllerDelegate {
    
    @IBOutlet var fanTableView: UITableView!
    @IBOutlet var collectionView: UICollectionView!
    
    var parameters: [Parameters]?
    
    private var dampersArray: [DamperParams]?
    
    private let tableViewCellId = "fanCell"
    
    private var popoverController: UIPopoverPresentationController?
    
    private let appDelegate = UIApplication.shared.delegate as? AppDelegate
    
    private var dataTimer: Timer?
    
    private func startDataTimer () {
        if self.dataTimer == nil {
            self.dataTimer =  Timer.scheduledTimer(
                timeInterval: TimeInterval(3.0),
                target      : self,
                selector    : #selector(fetchData),
                userInfo    : nil,
                repeats     : true)
        }
    }
    
    private func stopDataTimer() {
        if self.dataTimer != nil {
            self.dataTimer?.invalidate()
            self.dataTimer = nil
        }
    }
    
    
    //MARK:- UIVIEWCONTROLLER LIFE CYCLE METHODS
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNavigationItem()
        startDataTimer()
        
        NotificationCenter.default.addObserver(self, selector: #selector(appDidEnterBackground), name:NSNotification.Name.UIApplicationDidEnterBackground, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(appWillEnterForeground), name:NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
        
    }
    
    @objc func appDidEnterBackground() {
        stopDataTimer()
    }
    
    @objc func appWillEnterForeground() {
        startDataTimer()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.setHidesBackButton(true, animated: false)
        NotificationCenter.default.addObserver(self, selector: #selector(deviceOrientationDidChange), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    @objc func deviceOrientationDidChange(_ notification: Notification) {
        collectionView.reloadData()
    }
    
    //MARK: SetUp UI
    
    private func setNavigationItem() {
        navigationItem.title = "Fresh Air"
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white,.font:UIFont.systemFont(ofSize: 17.0, weight: .regular)]
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Machines", style: .plain, target: self, action: #selector(handleMachinesList))
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Dampers", style: .plain, target: self, action: #selector(getDampers))
        
    }
    
    @objc func handleMachinesList() {
        self.navigationController?.navigationBar.isUserInteractionEnabled = false
        
        let leftVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MACHINES")
        self.navigationController?.pushViewController(leftVC, animated: false)
    }
    
    @objc func getDampers() {
        self.navigationController?.navigationBar.isUserInteractionEnabled = false
        
        let transition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionMoveIn
        transition.subtype = kCATransitionFromLeft
        self.navigationController?.view.layer.add(transition, forKey: nil)
    }
    
    private func addSwipeGestures(){
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeRight.direction = UISwipeGestureRecognizer.Direction.right
        self.view.addGestureRecognizer(swipeRight)
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeLeft.direction = UISwipeGestureRecognizer.Direction.left
        self.view.addGestureRecognizer(swipeLeft)
        
        
       /*  let swipeUp = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeUp.direction = UISwipeGestureRecognizer.Direction.up
        self.view.addGestureRecognizer(swipeUp)
        self.animationView.addGestureRecognizer(swipeUp)
        swipeUp.delegate = horizontalCollectionView as? UIGestureRecognizerDelegate
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeDown.direction = UISwipeGestureRecognizer.Direction.down
        self.view.addGestureRecognizer(swipeDown)
        self.animationView.addGestureRecognizer(swipeDown)
        swipeDown.delegate = (horizontalCollectionView as? UIGestureRecognizerDelegate)*/
    }
    
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {

            switch swipeGesture.direction {
                
            case UISwipeGestureRecognizer.Direction.right:
                //right view controller
                
                let leftVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DAMPERS")
                let transition = CATransition()
                transition.duration = 0.5
                transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                transition.type = kCATransitionMoveIn
                transition.subtype = kCATransitionFromLeft
                self.navigationController?.view.layer.add(transition, forKey: nil)
                self.navigationController?.pushViewController(leftVC, animated: false)
                
            case UISwipeGestureRecognizer.Direction.left:
                //left view controller
                let rightVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MACHINES")
                self.navigationController?.pushViewController(rightVC, animated: true)
            /*case UISwipeGestureRecognizer.Direction.up:
                upButtonAction(self)
            case UISwipeGestureRecognizer.Direction.down:
                downButtonAction(self) */
            default:
                break
            }
        }
    }
   
    
    @objc func handleLogout() {
        
        let alert = UIAlertController(title: "", message: "Do you want to logout?", preferredStyle: .alert)
        let ok = UIAlertAction(title: "YES", style: .default, handler: { action in
            
            UserDefaults().removeValueForKey(key: .isLoggedIn)
            UserDefaults().removeValueForKey(key: .baseUrl)
            
            self.stopDataTimer()
            
            self.navigationController?.popToRootViewController(animated: true)
        })
        let cancel = UIAlertAction(title: "NO", style: .default, handler: { action in
            alert.dismiss(animated: true)
        })
        alert.addAction(ok)
        alert.addAction(cancel)
        present(alert, animated: true)
        
    }
    

    @IBAction func switchViewTapped(_ sender: Any) {
        
        let indexPath = IndexPath(item: 0, section: 0)
        let cell = fanTableView.cellForRow(at: indexPath) as! FanCell
        
        if cell.fanSwitch.isOn {
            fanExecutionAPI(level: "0", type: "Switch")
        } else {
            fanExecutionAPI(level: "4", type: "Switch")
        }
        
    }
    
    
    
    
}


//MARK: - Collectionview delegate

extension DashboardVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        if self.parameters == nil || self.parameters?.count == 0 {
            collectionView.setEmptyMessage("No data found")
            return 0
        } else {
            return 1
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.parameters != nil ? 5 : 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        var cell = UICollectionViewCell()
        cell  = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionCell", for: indexPath)
        
        cell.layer.borderColor = UIConstants().CELL_BORDERCOLOR
        cell.layer.borderWidth = 0.3
        cell.backgroundColor = UIConstants().CELL_BGCOLOR
        
        let cellImageView:UIImageView = cell.contentView.viewWithTag(1) as! UIImageView
        let cellValue:UILabel = cell.contentView.viewWithTag(2) as! UILabel
        
        guard let parameterArray = parameters else { return cell }
        
        if let parameter = parameterArray.first {
            
            if indexPath.item == 0 {
                cellImageView.image = #imageLiteral(resourceName: "humidity")
                cellValue.text = parameter.humidity
            } else if indexPath.item == 1 {
                cellImageView.image = nil
                cellValue.text = parameter.vocGas
            } else if indexPath.item == 2 {
                cellImageView.image = nil
                cellValue.text = parameter.ppm
            } else if indexPath.item == 3 {
                cellImageView.image = #imageLiteral(resourceName: "indoorTemp")
                cellValue.text = parameter.temperature
            } else {
                cellImageView.image = #imageLiteral(resourceName: "outdoorTemp")
                cellValue.text = parameter.outTemp
            }
            
        }
        return cell
    }
    
    //MARK: - Collectionview flow layout
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width :CGFloat  = self.collectionView.frame.size.width;
        var cellSize:CGSize
        
        cellSize = CGSize(width: width*0.325, height: width*0.325)
        
        return cellSize
    }
    
    
}

//MARK: - Tableview delegate and datasource

extension DashboardVC: UITableViewDelegate, UITableViewDataSource {
    
    //MARK: - Tableview delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.parameters != nil ? 1 : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.fanTableView.dequeueReusableCell(withIdentifier: tableViewCellId, for: indexPath) as! FanCell
        
        guard let parameterArray = parameters else { return cell }
        
        if let parameter = parameterArray.first {
            cell.parameter = parameter
        }
        
        
        
        cell.levelButtonTapped = { (fanCell) in
            
            let sb = UIStoryboard(name: "Main", bundle: nil)
            let popOverVC: PopOverVC = sb.instantiateViewController(withIdentifier: "POPOVER") as! PopOverVC
            
            popOverVC.modalPresentationStyle = .popover
            popOverVC.preferredContentSize = CGSize(width: 80, height: 200)
            popOverVC.delegate = self
            
            let controller = popOverVC.popoverPresentationController
            
            controller?.delegate = self
            controller?.permittedArrowDirections = [.right]
            
            controller?.sourceView = cell.levelButton
            controller?.sourceRect = cell.levelButton.bounds
            
            self.present(popOverVC, animated: true, completion: nil)
            
        }
        
        return cell
    }
    
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return .none
    }
    
    //MARK: - Tableview datasource
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UIScreen.main.bounds.size.height/13
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        print(indexPath)
    }
    
    
}

//MARK: - Fetch Data

extension DashboardVC: isAbleToReceiveData {
    
    @objc func fetchData() {
        
        NetworkingServices.shared.getWebServiceswithUrl("/getdata.cmd") {(result:  (Result<[Parameters], NetworkingServices.APIServiceError>)) in
            
            SVProgressHUD.dismiss()
            
            switch(result) {
                
            case .success(let response):
                
                self.parameters = response
                
                DispatchQueue.main.async {
                    self.collectionView.reloadData()
                    self.fanTableView.reloadData()
                }
                
                
            case .failure(let error):
                
                switch(error) {
                    
                case .noNetwork:
                    print("No Network")
                    //set count
                    // self.showAlertWithTitle("No Network", errorMessgage: "Please check your connection.")
                    
                default:
                    print(error)
                }
                
            }
        }
    }
    
    func fanExecutionAPI(level: String, type: String) {
        
        NetworkingServices.shared.executeFanUrl(withFanLevel: level) { (result:  (Result<String, NetworkingServices.APIServiceError>)) in
            
            switch (result) {
                
            case .success( _):
                
                DispatchQueue.main.async {
                    let indexPath = IndexPath(item: 0, section: 0)
                    let cell = self.fanTableView.cellForRow(at: indexPath) as! FanCell
                    
                    if level == "0" {
                        cell.fanSwitch.setOn(false, animated: true)
                        cell.levelButton.setTitle("Level 0", for: .normal)
                    } else {
                        cell.fanSwitch.setOn(true, animated: true)
                        cell.levelButton.setTitle("Level \(level)", for: .normal)
                    }
                }
                
                
            case .failure(let error):
                
                print("Error executing fan command: \(error)")
            }
        }
        
    }
    
}


/* SVProgressHUD.show()
 
 NetworkingServices.shared.getWebServiceswithUrl("/getdmprs.cmd") { (result:  (Result<[DamperParams], NetworkingServices.APIServiceError>)) in
 
 SVProgressHUD.dismiss()
 
 switch (result) {
 
 case .success(let response):
 
 DispatchQueue.main.async {
 self.dampersArray = response
 self.performSegue(withIdentifier: "goToDampers", sender: self)
 }
 
 case .failure(let error):
 
 switch(error) {
 
 case .noNetwork:
 DispatchQueue.main.async {
 self.showAlertWithTitle("No Network", errorMessgage: "Please check your connection.")
 }
 default:
 DispatchQueue.main.async {
 self.showAlertWithTitle("Oops", errorMessgage: "Could not reach server.")
 }
 }
 
 
 print("Error getting dampers \(error)")
 
 }
 
 } */
