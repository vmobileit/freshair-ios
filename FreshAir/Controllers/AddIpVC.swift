
//
//  AddIpViewController.swift
//  FreshAir
//
//  Created by Easyway_Mac2 on 18/12/18.
//  Copyright © 2018 Easyway_Mac2. All rights reserved.
//

import UIKit

class AddIpViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        setNavigationBar()
    }
    
    private func setNavigationBar(){
        navigationItem.title = "Add Machine Ip"
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white,.font:UIFont.systemFont(ofSize: 17.0, weight: .semibold)]
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(dismissView))
        
    }
  
    @objc func dismissView(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func saveMachineIp(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
}
