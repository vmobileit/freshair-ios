//
//  MachinesVC.swift
//  FreshAir
//
//  Created by Easyway_Mac2 on 17/12/18.
//  Copyright © 2018 Easyway_Mac2. All rights reserved.
//

import UIKit

class MachineIpListVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    let cellId = "MachineCell"
    
    
    @IBOutlet var machineListTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setNavigationBar()
    }
    
    private func setNavigationBar() {
        navigationItem.title = "Machine List"
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white,.font:UIFont.systemFont(ofSize: 17.0, weight: .semibold)]
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "addIcon"), style: .plain, target: self, action: #selector(addMachineIp))
        
        let backImage = UIImage(named: "backarrow")
        let backButton = UIButton(type: .custom)
        backButton.frame = CGRect(x: 0, y: 0, width: 60, height: 44)
        backButton.addTarget(self, action: #selector(self.navigationBackButtonAction), for: .touchUpInside)
        backButton.setImage(backImage, for: .normal)
        backButton.contentHorizontalAlignment = .left
        let back = UIBarButtonItem(customView: backButton)
        navigationItem.leftBarButtonItem = back
    }
    
    @objc func addMachineIp() {
        let addIpVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ADDIP")
        self.navigationController?.pushViewController(addIpVC, animated: true)
    }
    
    private func addGestureRecognizer(){
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.navigationBackButtonAction))
        swipeRight.direction = .right
        view.addGestureRecognizer(swipeRight)
        machineListTableView.addGestureRecognizer(swipeRight)
    }
    
    @objc func navigationBackButtonAction() {
        navigationController?.popViewController(animated: false)
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        machineListTableView.setEmptyMessage("No Machines found")
        return 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = machineListTableView.dequeueReusableCell(withIdentifier: cellId)
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            
        }
    }
}
