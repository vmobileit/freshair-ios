//
//  PopOverVC.swift
//  FreshAir
//
//  Created by Easyway_Mac2 on 16/04/19.
//  Copyright © 2019 Easyway_Mac2. All rights reserved.
//

import UIKit

protocol isAbleToReceiveData: class {
    func fanExecutionAPI(level: String, type: FanActionType)  //data: string is an example parameter
}

class PopOverVC: UIViewController {
    
    weak var delegate: isAbleToReceiveData?

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func levelSelected(_ sender: UIButton) {
        let selectedLevel = String(sender.tag - 1)
        
        dismiss(animated: true, completion: {
            self.delegate?.fanExecutionAPI(level: selectedLevel, type: .regulator)
        })
    }
}
