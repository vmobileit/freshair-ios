//
//  MachinesVC.swift
//  FreshAir
//
//  Created by Easyway_Mac2 on 17/12/18.
//  Copyright © 2018 Easyway_Mac2. All rights reserved.
//

import UIKit

class MachineIpListVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet var machineListTableView: UITableView!
    
    let cellId = "MachineCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setNavigationBar()
    }
    
    private func setNavigationBar(){
        navigationItem.title = "Machine List"
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white,.font:UIFont.systemFont(ofSize: 17.0, weight: .semibold)]
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "addIcon"), style: .plain, target: self, action: #selector(addMachineIp))
    }
    
    @objc func addMachineIp(){
        let addIpVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ADDIP")
        self.navigationController?.pushViewController(addIpVC, animated: true)
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = machineListTableView.dequeueReusableCell(withIdentifier: cellId)
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            
        }
    }
}
