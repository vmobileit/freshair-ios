//
//  DamperVC.swift
//  FreshAir
//
//  Created by Easyway_Mac2 on 25/06/18.
//  Copyright © 2018 Easyway_Mac2. All rights reserved.
//

import UIKit
import SVProgressHUD

class DamperVC: UIViewController {
    
    //MARK: - IBOutlets
    @IBOutlet var damperTableView: UITableView!
    
    //MARK: - Internal Parameters
    private var dampersArray: [DamperParams]? {
        didSet {
            self.damperTableView.reloadData()
        }
    }
    private let viewModel = DampersViewModel()
    
    //MARK: - View Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationItem()
        addGestureRecognizer()
   
        SVProgressHUD.show()
        
        ///Delegate the view model
        viewModel.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        viewModel.startDataTimer()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        viewModel.stopDataTimer()
    }
    
    //MARK: - UI Setup
    private func setNavigationItem() {
        let view1 = UIView(frame: CGRect(x: 0, y: 2, width: 50, height: 40))
        let logoItem:UIButton = UIButton(type: .roundedRect) as UIButton
        logoItem.frame = CGRect(x: 0, y: 3, width: 50, height: 35)
        logoItem.isEnabled = false
        logoItem.setBackgroundImage(#imageLiteral(resourceName: "aegis"), for: UIControl.State())
        view1.addSubview(logoItem)
        let logoButton = UIBarButtonItem (customView: view1)
        self.navigationItem.leftBarButtonItem = logoButton;
        
        navigationItem.title = "Dampers"
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white,.font:UIFont.systemFont(ofSize: 17.0, weight: .semibold)]
        
        let backImage = UIImage(named: "backarrow")
        let backButton = UIButton(type: .custom)
        backButton.frame = CGRect(x: 0, y: 0, width: 60, height: 44)
        backButton.addTarget(self, action: #selector(self.navigationBackButtonAction), for: .touchUpInside)
        backButton.setImage(backImage, for: .normal)
        backButton.contentHorizontalAlignment = .right
        let back = UIBarButtonItem(customView: backButton)
        navigationItem.rightBarButtonItem = back
    }
    
    private func addGestureRecognizer() {
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.navigationBackButtonAction))
        swipeLeft.direction = .left
        view.addGestureRecognizer(swipeLeft)
        damperTableView.addGestureRecognizer(swipeLeft)
    }

    @objc func navigationBackButtonAction() {
        let transition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.reveal
        transition.subtype = CATransitionSubtype.fromRight
        navigationController?.view.layer.add(transition, forKey: nil)
        navigationController?.popViewController(animated: false)
    }
}

//MARK: - Tableview Delegate and Datasource
extension DamperVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if dampersArray?.count == 0 {
            damperTableView.setEmptyMessage("No dampers found")
            return 0
        }
        
        damperTableView.backgroundView = nil
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return dampersArray?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = damperTableView.dequeueReusableCell(withIdentifier: "damperCell") as! DamperCell
        cell.delegate = self
        
        cell.damperParam = dampersArray?[indexPath.row]
        
        return cell
    }
}

//MARK: - Result Protocol
extension DamperVC: ResultProtocol {
    func success(_ responseData: Data) {
        dampersArray = viewModel.parseDampersResponse(of: responseData)
    }
    
    func error(_ message: String) {
        
    }
}

//MARK: - Damper Cell Delegate
extension DamperVC: DamperCellDelegate {
    func didTapSwitchView(sender: DamperCell) {
        sender.damperSwitch.isOn = !sender.damperSwitch.isOn
    }
}
