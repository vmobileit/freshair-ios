//
//  DampersViewModel.swift
//  FreshAir
//
//  Created by Easyway_Mac2 on 14/02/20.
//  Copyright © 2020 Easyway_Mac2. All rights reserved.
//

import Foundation

class DampersViewModel {
    weak var delegate: ResultProtocol?
    
    //MARK: - Internal Parameters
    private var dataTimer: Timer?
    
    private var baseUrl: String = {
        if let url = UserDefaults().getValueForKey(key: .baseUrl) as? String {
            return url
        } else {
            fatalError("Base Url not found")
        }
    }()
    
    private let webService = WebService()
    
    //MARK: - Data Parse
    func parseDampersResponse(of data: Data) -> [DamperParams]? {
        do {
            let decoder = JSONDecoder()
            let decodedResponse = try decoder.decode([DamperParams].self, from: data)
            return decodedResponse
        } catch let error {
            print(error)
        }
        return nil
    }
    
    //MARK: - Data Timer
      func startDataTimer() {
          if dataTimer == nil {
              dataTimer =  Timer.scheduledTimer(
                  timeInterval: TimeInterval(3.0),
                  target      : self,
                  selector    : Selector.fetchData,
                  userInfo    : nil,
                  repeats     : true)
          }
      }
      
      func stopDataTimer() {
          if dataTimer != nil {
              dataTimer?.invalidate()
              dataTimer = nil
          }
      }
    
    @objc fileprivate func fetchData() {
        webService.delegate = delegate
        webService.getData(for: baseUrl, serviceType: .dampers)
    }
}

fileprivate extension Selector {
    static  let  fetchData = #selector(DampersViewModel.fetchData)
}
