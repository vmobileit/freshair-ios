//
//  Parameters.swift
//  FreshAir
//
//  Created by Easyway_Mac2 on 12/04/19.
//  Copyright © 2019 Easyway_Mac2. All rights reserved.
//

import Foundation

/*
 [
 {"FAN":"0","PPM":"0","temp":"0","humidity":"0","outtemp":"0","humidity":"0"}
 ]
 */
struct Parameters: Decodable {
    let temperature: String?
    let humidity: String?
    let outTemp: String?
    let ppm: String?
    let fanValue: String?
    let vocGas: String?
    
    
    enum CodingKeys: String, CodingKey {
        case temperature = "temp"
        case humidity
        case outTemp = "outtemp"
        case ppm = "PPM2.5"
        case fanValue = "FAN"
        case vocGas = "VOC Gas"
    }
}


/*[
 {"seq":"0","id":"1","boardid":"1234567","relay":"1","name":"Master Bed Room","status":"0"},
 {"seq":"1",,"id":"2","boardid":"1234567","relay":"2","name":" Main Hall","status":"0"},
 {"seq":"2",,"id":"3","boardid":"1234567","relay":"3","name":" MD Room","status":"0"}
 ]*/

struct DamperParams: Decodable {
    
    let seq: String?
    let id: String?
    let boardId: String?
    let relay: String?
    let name: String?
    let status: String?
    
    
    enum CodingKeys: String, CodingKey {
        case seq
        case id
        case boardId = "boardid"
        case relay
        case name
        case status
    }
}
